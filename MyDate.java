/* A22538 - Nguyen Thi Ngoc */
import java.util.Scanner;

public class MyDate {
    private int date;
    private int month;
    private int year;
    
     public MyDate() {
    }

    public MyDate(int date, int month, int year) {
        this.date = date;
        this.month = month;
        this.year = year;
    }

    public void input(){
        int d,m,y;
        Scanner sc = new Scanner(System.in);
        System.out.print("Input Day: ");
        d = sc.nextInt();
        System.out.print("Input Month: ");
        m = sc.nextInt();
        System.out.print("Input Year: ");
        y = sc.nextInt();
        System.out.println("==============================");
        System.out.println("Day/Month/Year: " + d + "/" + m + "/" + y);
    }

    @Override
    public String toString() {
        return "MyDate{" + "date=" + date + ", month=" + month + ", year=" + year + '}';
    }
    public static void main(String[] args){
        MyDate myDate = new MyDate();
        myDate.input();
        
        MyDate myDate1 = new MyDate(22,10,1995);
        System.out.println("" + myDate1.toString());
    }
}